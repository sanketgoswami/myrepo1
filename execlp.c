#include<stdio.h>
#include<sys/wait.h>
#include<sys/types.h>

int main()
{
  pid_t ch_pid;
  int status;

  ch_pid = fork();
  printf("ch_pid : %d\n",ch_pid);
  if(!ch_pid){
    execlp("ls","myls");
    printf("This will not be printed\n");
  } else {
    wait(&status);
    printf("ch_status : %d\n",status);
  }
  return 0;
}
