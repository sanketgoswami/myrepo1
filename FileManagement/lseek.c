// 
// This is an example to find the size of a file using lseek system call.
//

#include<stdio.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<errno.h>

int main(int argc, char *argv[]){

	int fd1;
	int size;
	char *buff = "Usage : ./a.out <filename>\n";
	
	if(argc == 1){
		write(1, buff, strlen(buff));
		return 0;
	}

	if(argc > 2){
		buff = "Invalid arguments\n";
		write(1, buff, strlen(buff));
	}

	fd1 = open(argv[1], O_RDWR | O_CREAT);

	if(fd1 == -1){
		printf("Could not open the file.");
		return errno;
	}

	size = lseek(fd1, 0, SEEK_END) - lseek(fd1, 0, SEEK_SET);
	printf("file size = %d\n", size);

	return 0;
}
