//
//
//

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char *argv[]){
	int fd1;
	struct flock lock;
	if(argc == 1){
		printf("Usage : ./a.out <filename>\n");
		return 0;
	}
	
	fd1 = open(argv[1], O_RDWR);

	lock.l_type = F_WRLCK;
	lock.l_whence = SEEK_SET;
	lock.l_start = 0;
	lock.l_len = 100;
	lock.l_pid = getpid();

	int rv = fcntl(fd1, F_SETLK, &lock);
	if(rv == -1){
		printf("Failed to set lock with errno %d\n",errno);
		return rv;
	}
	printf("File successfully locked\n");
	return 0;
}
