//
// Program to mimic more utility of UNIX shell 
//

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string.h>

#define MAX_LENGTH 512
void more(char*, int);
int  FileProgress(FILE*);
void set_terminal_mode(void);
struct termios saved_attrs;

int main(int argc, char *argv[]) 
{
  int i, totalFiles;
  char *arg;

  //Set terminal mode to non canonical and non echo
  set_terminal_mode();

  if(argc == 1){
    //printf("more: bad usage\nTry 'more --help' fore more information.\n");
    more(NULL,1);
    return 0;
  }


  //Parse all [OPTIONS] given to the more command
  for(i = 1; i < argc; i++){
    arg = argv[i];
    
    if(arg[0] == '-'){
      continue;
    } else {
      break;
    }    
  }
  
  //Total number of files
  totalFiles = argc -i;  

  //Display each file 
  while(i < argc){
    more(argv[i], totalFiles);
    i++;
  }
  return 0;
}

void ResetTerminalModes(void){
  tcsetattr(STDIN_FILENO, TCSANOW, &saved_attrs);
}

void set_terminal_mode(void)
{

  struct termios term_attrs;

  //Make sure stdin is a terminal.
  if(!isatty(STDIN_FILENO)){
    printf("Not a terminal!\n");
    exit(EXIT_FAILURE);
  }

  //Save terminal attributes so we can restore them later.
  tcgetattr(STDIN_FILENO, &saved_attrs); 
  atexit(ResetTerminalModes);
  
  //Set the non-canonical non-echo mode
  term_attrs = saved_attrs;
  term_attrs.c_lflag &= ~(ICANON | ECHO); //Clear ICANON and ECHO
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &term_attrs);

}

int get_term_pagelen()
{
  int PageLen;
  struct winsize size;

  ioctl(STDIN_FILENO, TIOCGWINSZ, &size);
  PageLen = size.ws_row;

  return PageLen;
}

void open_editor(char* file, int line)
{
  char str_line[10];
  int  status;
  if(fork()){
    
    wait(&status);
    return;
  }
  sprintf(str_line, "%d", line);
  status = execlp("vi", "vi", "-c", str_line, file);
  printf("execlp status : %d\n",status); // will not print
  exit(0);
}
int search_file(FILE *fp, char *search)
{
  char line[512];
  int org;
  int count = 0;

  org = ftell(fp);
  while(fgets(line, 512, fp)){
    count++;
    if(strstr(line, search)){
      break;  
    }
  }
  fseek(fp, org, SEEK_SET);
  return count;
}

void more(char *filename, int totalFiles){
  
  char  line[MAX_LENGTH];
  int   PageLen, RemainingPageLen = 0;
  FILE *fp_tty = fopen("/dev/tty","r");
  FILE *fp;
  char  input;
  int   lines = 0;

  if(filename == NULL){
    fp = stdin;
  } else {
    fp = fopen(filename, "r");
  }

  PageLen = get_term_pagelen();
  RemainingPageLen = PageLen;

  if(totalFiles > 1){
    printf("::::::::::::::\n");
    printf("%s\n",filename); 
    printf("::::::::::::::\n");
    RemainingPageLen -= 3;
  }  
 

  while(fgets(line, MAX_LENGTH, fp) != NULL){

    fputs(line, stdout);
    RemainingPageLen--;
    lines++;

    if(RemainingPageLen == 1){  

      printf("\e[7m--more--(%d%%)\033[m",FileProgress(fp));      

      while(RemainingPageLen == 1){

        input = getc(fp_tty);

        if(input == '\n'){
          RemainingPageLen++;
          printf("\033[2K \033[1G");
        }
        else if(input == ' '){
          printf("\033[2K \033[1G");
          RemainingPageLen = get_term_pagelen(); 
        }
        else if(input == 'q'){
          printf("\033[2K \033[1G");
          fclose(fp);
          exit(0);
        }
        else if(input == 'v'){
          int edit_line = lines - (PageLen/2);
          printf("\033[2K \033[1G");
          printf("vi -c %d %s-----------------------\n",edit_line,filename);
          open_editor(filename, edit_line);
        }
        else if(input == '/'){
          char search[100];
          int  i = 0;

          ResetTerminalModes();
          printf("\033[2K \033[1G/");
          while((search[i++] = getc(fp_tty)) != '\n');
          set_terminal_mode();
          printf("\033[1A \033[2K \033[1G");
          search[--i] = '\0';
          RemainingPageLen = get_term_pagelen(); 

          i = search_file(fp, search);
          if(i>3){    
            printf("...skipping\n");
            RemainingPageLen--;
            while(i-->3) fgets(line, MAX_LENGTH, fp);
          } else {
            RemainingPageLen -= (3 - i);
          }          
        }
      }
    }
  }
  fclose(fp);
  return;
}

int FileProgress(FILE *fp){
  int cur, size;
  int progress;

  cur = ftell(fp);
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, cur, SEEK_SET);
  
  progress = (int)(cur*100)/size;
  return progress;
}
