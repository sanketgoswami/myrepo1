#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

extern int etext, edata, end;

int I = 1; //initialized variable (.data section)
int J;     //uninitialized variable (.bss section)

int main(int argc, char *argv[]){
	int i;          //Stack variable
	int *ii = NULL; // stack variable

	printf("Page size   : %d\n", getpagesize());
	printf("Size of int : %ld\n", sizeof(int));
	printf("Size of addr: %ld\n\n", sizeof(ii));

	printf("&I (initialized var)  : %p\n", &I);
	printf("&edata                : %p\n\n", &edata);

	printf("&J (uninitialized var): %p\n", &J);
	printf("&end                  : %p\n\n", &end);

	printf("main                  : %p\n", main);
	printf("&etext                : %p\n\n", &etext);

	ii = (int*) malloc(sizeof(int));
	printf("ii (heap address)     : %p\n\n",ii);

	//these stack variables are part of main Function Stack Frame
	//First local variables are stored in stack 
	//then function arguments are store

	printf("&argc : %p\n", &argc);
	printf("&i    : %p\n", &i);
	printf("&ii   : %p\n\n", &ii);

	return 0;
}
