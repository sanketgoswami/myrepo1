//
//
//

#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <errno.h>

int main(){
  pid_t ch_pid;

  ch_pid = fork();
  if(ch_pid == 0){
    execl("/bin/ls", "myls", "-l",NULL);
    perror("execl failed");
  } else {
    int status;
    int rv = wait(&status);
    printf("This is parent\n");
    return rv;
  }
  return 0;
}
