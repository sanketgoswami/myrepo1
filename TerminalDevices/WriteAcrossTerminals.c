//
//
//

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>


int main(int argc, char *argv[]){
  int fd1;
  char buffer[100] = {0};

  if(argc == 1){
    printf("Invalid argument count\nUsage : ./a.out <terminal file>\n");
    return 0;
  }
  fd1 = open(argv[1], O_RDWR);
  if(fd1 < 0){
    perror("Couldn't open file ");
    exit(errno);
  }
  dup2(fd1, 1);
  printf("This will be printed in the given terminal file.\n");
  //scanf("%s", buffer);
  close(fd1);
  close(1);  
  return 0;
}
