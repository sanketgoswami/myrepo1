//
//
//

#include <stdio.h>
#include <unistd.h>
#include <wait.h>

int main(){
  pid_t ch_pid;

  ch_pid = fork();
  if(ch_pid == 0){
    execlp("ls","Sanket", "-l",NULL);
    printf("This will not be printed.\n");
  } else {
    int status;
    int rv = wait(&status);
    printf("This is parent\n");
  }
  return 0;
}
